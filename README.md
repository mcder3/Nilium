# Nilium Plasma Theme

Nilium is a dark theme designed from scratch for Plasma 6.x

Licensed under **[CC BY-SA 4.0](https://gitlab.com/mcder3/Nilium/-/tree/main/LICENSE.md "CC BY-SA 4.0")**

For more information visit the Nilium section at **[opendesktop](https://www.opendesktop.org/p/1226329/ "opendesktop")**
